#pragma once
#include "impl/tuple_ext_impl.h"


namespace tuple_ext{

// ============================= head_t =============================

template<is_tuple T>
requires ( std::tuple_size_v<T> != 0 )
using head_t = typename impl::head<T>::type;


// ============================= tail_t =============================

template<is_tuple T>
using tail_t = typename impl::tail<T>::type;

// ============================= take_t =============================

template<std::size_t N,typename T>
using take_t = typename impl::take<N,T>::type;

// ============================= drop_t =============================

template<std::size_t N,typename T>
using drop_t = typename impl::drop<N,T>::type;


// ============================= has_type_v =============================

template<typename T,is_tuple Tp>
inline constexpr bool has_type_v = impl::has_type<T, Tp>::value;


// ============================= reverse_t =============================

template<is_tuple Tp>
using reverse_t = typename impl::reverse<Tp>::type;


// ============================= unique_t =============================

template<is_tuple Tp>
using unique_t = typename impl::unique<Tp>::type;


// ============================= remove_t =============================

template<is_tuple Tp1,is_tuple Tp2>
using remove_t = typename impl::remove<Tp1>::from<Tp2>::type;


// ============================= repeat_t =============================

template<std::size_t N,typename T>
using repeat_t = typename impl::repeat<N,T>::type;


// ============================= elem_index =============================

template<typename T,is_tuple Tp2>
constexpr std::size_t elem_index_v = impl::elem_index<0,T,Tp2>::value;


// ============================= elem_at_t =============================

template<std::size_t N,is_tuple Tp>
requires ( N < std::tuple_size_v<Tp> )
using elem_at_t = impl::elem_at<N,Tp>::type;


// ============================= inter_t =============================

template<is_tuple Tp1,is_tuple Tp2>
using inter_t = typename impl::inter<Tp1>::with<Tp2>::type;


// ============================= fst_t and snd_t =============================

template<is_pair Tp>
using fst_t = typename impl::fst<Tp>::type;

template<is_pair Tp>
using snd_t = typename impl::snd<Tp>::type;


// ============================= zip_t =============================

template<is_tuple Tp1,is_tuple Tp2>
using zip_t = typename impl::zip<Tp1>::with<Tp2>::type;


// ============================= unzip_t =============================

template<is_tuple_of_pair Tp>
using unzip_t = typename impl::unzip<Tp>::type;


// ============================= filter_t =============================

template<template<typename T> typename F, is_tuple Tp>
using filter_t = typename impl::filter<F,Tp>::type;


// ============================= map_t =============================

template<template<typename T> typename F, is_tuple Tp>
using map_t = typename impl::map<F,Tp>::type;


// ============================= foldr_t =============================

template<
  template<typename,typename> typename F, // F(TX,TY)
  typename TY,
  is_tuple TXs
>
using foldr_t = typename impl::foldr<F,TY,TXs>::type;


// ============================= foldl_t =============================

template<
  template<typename,typename> typename F, // F(TX,TY)
  typename TY,
  is_tuple TXs
>
using foldl_t = typename impl::foldl<F,TY,TXs>::type;

} // namespace tuple_ext
