#pragma once
#include <type_traits>
#include <tuple>

namespace tuple_ext
{

// ============================= is_tuple =============================

namespace impl
{
  template<typename T>
  struct is_tuple_s : std::false_type {};

  template<typename... Ts>
  struct is_tuple_s<std::tuple<Ts...>> : std::true_type {};
}

template<typename T>
concept is_tuple = impl::is_tuple_s<T>::value;

// ============================= is_pair =============================

namespace impl
{
  template<typename T>
  struct is_pair_s : std::false_type {};

  template<typename T0,typename T1>
  struct is_pair_s<std::pair<T0,T1>> : std::true_type {};
}

template<typename T>
concept is_pair = impl::is_pair_s<T>::value;

// ============================= is_tuple_of_pair =============================

namespace impl
{
  template<typename T>
  struct is_tuple_of_pair_s : std::false_type {};

  template<is_pair... Ts>
  struct is_tuple_of_pair_s<std::tuple<Ts...>> : std::true_type {};
}

template<typename T>
concept is_tuple_of_pair = impl::is_tuple_of_pair_s<T>::value;

// ============================= concat_t =============================

template<typename...Tps>
requires ( is_tuple<Tps> && ... )
using concat_t = decltype(std::tuple_cat(std::declval<Tps>()...));

namespace impl
{

// ============================= head =============================

template<is_tuple> struct head;

template<typename T,typename... Ts>
struct head<std::tuple<T,Ts...>>
{
  using type = T;
};


// ============================= tail =============================

template<is_tuple> struct tail;

template<typename T,typename... Ts>
struct tail<std::tuple<T,Ts...>>
{
  using type = std::tuple<Ts...>;
};

// ============================= take =============================

template<std::size_t, is_tuple> struct take;

template<std::size_t N,typename T,typename... Ts>
struct take<N,std::tuple<T,Ts...>>
{
  using type = concat_t<std::tuple<T>,typename take<N-1,std::tuple<Ts...>>::type>;
};

template<std::size_t N>
struct take<N,std::tuple<>>
{
  using type = std::tuple<>;
};

template<typename T,typename... Ts>
struct take<0,std::tuple<T,Ts...>>
{
  using type = std::tuple<>;
};

// ============================= drop =============================

template<std::size_t, is_tuple> struct drop;

template<std::size_t N,typename T,typename... Ts>
struct drop<N,std::tuple<T,Ts...>>
{
  using type = typename drop<N-1,std::tuple<Ts...>>::type;
};

template<std::size_t N>
struct drop<N,std::tuple<>>
{
  using type = std::tuple<>;
};

template<typename T,typename... Ts>
struct drop<0,std::tuple<T,Ts...>>
{
  using type = std::tuple<T,Ts...>;
};

// ============================= has_type =============================

template<typename, is_tuple> struct has_type;

template<typename T, typename... Ts>
struct has_type<T,std::tuple<Ts...>>
{
  static constexpr bool value = (std::is_same<T,Ts>::value || ...);
};


// ============================= reverse =============================

template<is_tuple> struct reverse;

template<typename... Ts>
struct reverse<std::tuple<Ts...>>
{
  using type = std::tuple<>;
};

template<typename T,typename... Ts>
struct reverse<std::tuple<T,Ts...>>
{
  using type = concat_t<typename reverse<std::tuple<Ts...>>::type, std::tuple<T>>;
};


// ============================= unique =============================

template<is_tuple> struct runique;

template<typename... Ts>
struct runique<std::tuple<Ts...>>
{
  using type = std::tuple<Ts...>;
};

template<typename T,typename... Ts>
struct runique<std::tuple<T,Ts...>>
{
  using type = concat_t<
    std::conditional_t<
      impl::has_type<T,std::tuple<Ts...>>::value,
      std::tuple<>,
      std::tuple<T>
    >,
    typename runique<std::tuple<Ts...>>::type
  >;
};

template<is_tuple> struct unique;

template<typename... Ts>
struct unique<std::tuple<Ts...>>
{
  using type = typename reverse<
    typename impl::runique <
      typename impl::reverse<std::tuple<Ts...>>::type
    >::type
  >::type;
};


// ============================= remove =============================

template<is_tuple> struct remove;

template<typename ... T1s>
struct remove<std::tuple<T1s...>>
{
  template<typename Tp> struct from;

  template<typename... T2s>
  struct from<std::tuple<T2s...>>
  {
    using type = concat_t<
      typename std::conditional<
        impl::has_type<T2s,std::tuple<T1s...>>::value,
        std::tuple<>,
        std::tuple<T2s>
      >::type...
    >;
  };
};

// ============================= repeat =============================

template<std::size_t N,typename T>
struct repeat
{
  using type = concat_t<
    std::tuple<T>,
    typename repeat<N-1,T>::type
  >;
};

template<typename T>
struct repeat<0,T>
{
  using type = std::tuple<>;
};


// ============================= elem_index =============================

template<std::size_t,typename, is_tuple> struct elem_index;

template<std::size_t N,typename T, typename T0, typename... Ts> 
struct elem_index<N,T,std::tuple<T0,Ts...>>
{
  using type = std::conditional_t<
    std::is_same_v<T,T0>,
    std::integral_constant<std::size_t, N>,
    typename elem_index<1+N,T,std::tuple<Ts...>>::type
  >;

  constexpr static std::size_t value = type::value;
};

template<std::size_t N,typename T, typename... Ts> 
struct elem_index<N,T,std::tuple<Ts...>>
{
  using type = std::integral_constant<std::size_t, N>;
  constexpr static std::size_t value = N;
};


// ============================= elem_at =============================

template<std::size_t,is_tuple> struct elem_at;

template<std::size_t N,typename T0, typename... Ts> 
struct elem_at<N,std::tuple<T0,Ts...>>
{
  using type = typename elem_at<N-1,std::tuple<Ts...>>::type;
};

template<typename T0, typename... Ts> 
struct elem_at<0,std::tuple<T0,Ts...>>
{
  using type = T0;
};


// ============================= inter =============================

template<is_tuple> struct inter;

template<typename ... T1s>
struct inter<std::tuple<T1s...>>
{
  template<typename Tp> struct with;

  template<typename... T2s>
  struct with<std::tuple<T2s...>>
  {
    using type = concat_t<
      typename std::conditional<
        impl::has_type<T1s,std::tuple<T2s...>>::value,
        std::tuple<T1s>,
        std::tuple<>
      >::type...
    >;
  };
};


// ============================= zip =============================

template<is_tuple> struct zip;

template<typename... T1s>
struct zip<std::tuple<T1s...>>
{
  template<typename T2> struct with;

  template<typename... T2s>
  requires ( sizeof...(T1s) == sizeof...(T2s) )
  struct with<std::tuple<T2s...>>
  {
    using type = std::tuple<std::pair<T1s,T2s>... >;
  };
};

// ============================= fst =============================

template<typename> struct fst;

template<typename T1,typename T2>
struct fst<std::pair<T1,T2>>
{
  using type = T1;
};


// ============================= snd =============================

template<typename> struct snd;

template<typename T1,typename T2>
struct snd<std::pair<T1,T2>>
{
  using type = T2;
};


// ============================= unzip =============================

template<is_tuple_of_pair> struct unzip;

template<is_pair... Ts>
struct unzip<std::tuple<Ts...>>
{
  using type = std::pair<
    std::tuple<typename fst<Ts>::type...>,
    std::tuple<typename snd<Ts>::type...>
  >;
};


// ============================= map =============================

template<template<typename T> typename F, is_tuple> struct map;

template<template<typename T> typename F, typename... Ts>
struct map<F,std::tuple<Ts...>>
{
  using type = std::tuple<typename F<Ts>::type...>;
};


// ============================= filter =============================

template<template<typename T> typename F, is_tuple> struct filter;

template<
  template<typename> typename F,
  typename... Ts>
struct filter<F,std::tuple<Ts...>>
{
  using type = std::tuple<>;
};


// filter f (x:xs) | f x       = x : filter p xs
//                   otherwise = filter p xs
template<
  template<typename> typename F,
   typename T,
   typename... Ts>
struct filter<F,std::tuple<T,Ts...>>
{
  using type = concat_t<
    std::conditional_t<
      F<T>::value,
      std::tuple<T>,
      std::tuple<>
    >,
    typename filter<
      F,
      std::tuple<Ts...>
    >::type
  >;
};



// ============================= foldr =============================

template<template<typename,typename> typename F, typename TY, is_tuple TXs> struct foldr;

// foldr f x [] = x
template<
  template<typename,typename> typename F,
  typename TY,
  typename... TXs
>
struct foldr<F,TY,std::tuple<TXs...>>
{
  using type = TY;
};

// foldr f x (y:ys) = f x foldr f x ys
template<
  template<typename,typename> typename F, // F(X,TY)
  typename TY,
  typename X, typename... TXs
>
struct foldr<F,TY,std::tuple<X,TXs...>>
{
  using type = typename F<
    X,
    typename foldr<F,TY,std::tuple<TXs...>>::type
  >::type;
};


// ============================= foldl =============================

template<template<typename,typename> typename F, typename TY, is_tuple TXs> struct foldl;

// foldl f x [] = x
template<
  template<typename,typename> typename F,
  typename TY,
  typename... TXs
>
struct foldl<F,TY,std::tuple<TXs...>>
{
  using type = TY;
};

// foldl f x (y:ys) = f x foldl f x ys
template<
  template<typename,typename> typename F, // F(X,TY)
  typename TY,
  typename X, typename... TXs
>
struct foldl<F,TY,std::tuple<X,TXs...>>
{
  using type = typename foldl<
    F,
    typename F<X,TY>::type,
    std::tuple<TXs...>
  >::type;
};

} // namespace impl
} // namespace tuple_ext
